cd rust
cargo build --release

# Copy libraries
mkdir -p ${OPAM_SWITCH_PREFIX}/lib
cp target/release/librustc_bls12_381.a ${OPAM_SWITCH_PREFIX}/lib/
cp target/release/librustc_jubjub.a ${OPAM_SWITCH_PREFIX}/lib/

# Copy headers
mkdir -p ${OPAM_SWITCH_PREFIX}/include
cp rustc-bls12-381/include/*.h ${OPAM_SWITCH_PREFIX}/include
cp rustc-jubjub/include/*.h ${OPAM_SWITCH_PREFIX}/include
