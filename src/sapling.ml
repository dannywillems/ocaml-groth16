module Groth16_Sapling = Generic.MakeGroth16 (struct
  module G1 = Bls12_381.G1.Uncompressed
  module G2 = Bls12_381.G2.Uncompressed
  module GT = Bls12_381.Fq12
  include Bls12_381.Pairing
end)

type commitment_value = Jubjub.t

type commitment = Jubjub.BaseField.t

type ephemeral_public_key = Jubjub.t

type output_proof = Groth16_Sapling.proof

let output_proof_of_points = Groth16_Sapling.proof_of_points

(** TODO: parse the zcash params file. The type must be Generic.public_verify_key *)
let _sapling_verify_key_output = ()

(** TODO *)
let check_output commitment_value commitment ephemeral_public_key output_proof
    verifying_key =
  let (cv_x, cv_y) =
    ( Jubjub.get_u_coordinate commitment_value,
      Jubjub.get_v_coordinate commitment_value )
  in
  let (epk_x, epk_y) : Jubjub.BaseField.t * Jubjub.BaseField.t =
    ( Jubjub.get_u_coordinate ephemeral_public_key,
      Jubjub.get_v_coordinate ephemeral_public_key )
  in
  let public_inputs : Groth16_Sapling.public_input list =
    [ Groth16_Sapling.Scalar.of_bytes_exn (Jubjub.BaseField.to_bytes cv_x);
      Groth16_Sapling.Scalar.of_bytes_exn (Jubjub.BaseField.to_bytes cv_y);
      Groth16_Sapling.Scalar.of_bytes_exn (Jubjub.BaseField.to_bytes epk_x);
      Groth16_Sapling.Scalar.of_bytes_exn (Jubjub.BaseField.to_bytes epk_y);
      Groth16_Sapling.Scalar.of_bytes_exn (Jubjub.BaseField.to_bytes commitment)
    ]
  in
  Groth16_Sapling.verify_proof verifying_key output_proof public_inputs
