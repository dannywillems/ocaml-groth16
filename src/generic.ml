module type Groth16_sig = sig
  module Scalar : Ff.BASE

  module G1 : Ec.Elliptic_curve.T

  module G2 : Ec.Elliptic_curve.T

  (** Represent a R1CS circuit *)
  type r1cs_circuit

  val is_circuit_well_formed : r1cs_circuit -> bool

  (** Type of the verify key *)
  type public_verify_key

  (** A Groth16 proof *)
  type proof

  (** Build a proof value from the points A, B, C *)
  val proof_of_points : G1.t -> G2.t -> G1.t -> proof

  (** Types of the public inputs a_i, i in {0, ..., l} *)
  type public_input = Scalar.t

  (** Verify a proof given a verify key, a proof and a list of public input. Return true if the proof is correct *)
  val verify_proof : public_verify_key -> proof -> public_input list -> bool

  (** TODO *)
  val setup : r1cs_circuit -> unit
end

module MakeGroth16 (Pairing : Ec.Pairing.T) = struct
  module Scalar = Pairing.G1.Scalar
  module G1 = Pairing.G1
  module G2 = Pairing.G2

  type polynome = Scalar.t list

  type r1cs_circuit = { u_x : polynome; v_x : polynome; w_x : polynome }

  let is_circuit_well_formed circuit =
    let u_x_length = List.length circuit.u_x in
    let v_x_length = List.length circuit.v_x in
    let w_x_length = List.length circuit.w_x in
    u_x_length = v_x_length && w_x_length = v_x_length

  let setup (circuit : r1cs_circuit) =
    assert (is_circuit_well_formed circuit) ;
    let _alpha = Scalar.random () in
    let _beta = Scalar.random () in
    let _gamma = Scalar.random () in
    let _delta = Scalar.random () in
    let _x = Scalar.random () in
    ()

  type public_verify_key =
    { (* pairing result of alpha * beta *)
      alpha_g1_beta_g2 : Pairing.GT.t;
      (* opposite of gamma, exponentiated in G2 *)
      neg_gamma_g2 : Pairing.G2.t;
      (* opposite of delta, exponentiated in G2 *)
      neg_delta_g2 : Pairing.G2.t;
      (* (beta * u_i(x) + alpha * v_i(x) + w_i(x)) / gamma
         Computation result of the circuit, without the public parameters, already exponentiated in G1
      *)
      input_commitments : Pairing.G1.t list
    }

  type proof = { a : Pairing.G1.t; b : Pairing.G2.t; c : Pairing.G1.t }

  let proof_of_points a b c = { a; b; c }

  type public_input = Scalar.t

  (** Attempt to copy Bellman *)
  let verify_proof public_verify_key proof public_inputs =
    assert (
      List.length public_inputs + 1
      = List.length public_verify_key.input_commitments ) ;
    let (acc, pvk_ic_s) =
      ( List.hd public_verify_key.input_commitments,
        List.tl public_verify_key.input_commitments )
    in
    (* let's compute the sum a_i * ic_i in G1, preparing for the miller loop computation *)
    let acc =
      List.fold_left2
        (fun acc a_i g -> Pairing.G1.add acc (Pairing.G1.mul g a_i))
        acc
        public_inputs
        pvk_ic_s
    in
    (* and now the miller loop to get the pairing result *)
    let miller_loop =
      Pairing.miller_loop
        [ (proof.a, proof.b);
          (acc, public_verify_key.neg_gamma_g2);
          (proof.c, public_verify_key.neg_delta_g2) ]
    in
    Pairing.final_exponentiation_exn miller_loop
    = public_verify_key.alpha_g1_beta_g2
end
