module Groth16_Sapling : Generic.Groth16_sig

type commitment_value

type commitment

type ephemeral_public_key

type output_proof

val output_proof_of_points :
  Groth16_Sapling.G1.t ->
  Groth16_Sapling.G2.t ->
  Groth16_Sapling.G1.t ->
  output_proof

val check_output :
  commitment_value ->
  commitment ->
  ephemeral_public_key ->
  output_proof ->
  Groth16_Sapling.public_verify_key ->
  bool
