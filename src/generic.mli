module type Groth16_sig = sig
  module Scalar : Ff.BASE

  module G1 : Ec.Elliptic_curve.T

  module G2 : Ec.Elliptic_curve.T

  (** Represent a R1CS circuit *)
  type r1cs_circuit

  val is_circuit_well_formed : r1cs_circuit -> bool

  (** Type of the verify key *)
  type public_verify_key

  (** A Groth16 proof *)
  type proof

  (** Build a proof value from the points A, B, C *)
  val proof_of_points : G1.t -> G2.t -> G1.t -> proof

  (** Types of the public inputs a_i, i in {0, ..., l} *)
  type public_input = Scalar.t

  (** Verify a proof given a verify key, a proof and a list of public input. Return true if the proof is correct *)
  val verify_proof : public_verify_key -> proof -> public_input list -> bool

  (** TODO *)
  val setup : r1cs_circuit -> unit
end

module MakeGroth16 (Pairing : Ec.Pairing.T) :
  Groth16_sig
    with type Scalar.t = Pairing.G1.Scalar.t
     and type G1.t = Pairing.G1.t
     and type G2.t = Pairing.G2.t
