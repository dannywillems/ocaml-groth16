use jubjub::{AffinePoint, ExtendedPoint, Fq, Fr};
use libc::c_uchar;
use std::ops::Add;

const LENGTH_JUBJUB_AFFINE_POINT: usize = 32;
const LENGTH_JUBJUB_FR: usize = 32;
const LENGTH_JUBJUB_FQ: usize = 32;

fn write_jubjub_affine_point(buffer: *mut [u8; LENGTH_JUBJUB_AFFINE_POINT], g: AffinePoint) {
    let buffer = unsafe { &mut *buffer };
    for (d, s) in buffer.iter_mut().zip(g.to_bytes().iter()) {
        *d = *s;
    }
}

fn write_jubjub_coordinate(buffer: *mut [u8; LENGTH_JUBJUB_FQ], x: Fq) {
    let buffer = unsafe { &mut *buffer };
    for (d, s) in buffer.iter_mut().zip(x.to_bytes().iter()) {
        *d = *s;
    }
}

#[no_mangle]
pub extern "C" fn rustc_jubjub_check_bytes(
    g: *const [c_uchar; LENGTH_JUBJUB_AFFINE_POINT],
) -> bool {
    let g = unsafe { &*g };
    let g = AffinePoint::from_bytes(*g);
    if bool::from(g.is_none()) {
        false
    } else {
        let g = g.unwrap();
        // Checking if it is in the prime subgroup.
        if g == AffinePoint::identity() {
            true
        } else {
            bool::from(g.is_torsion_free())
        }
    }
}

#[no_mangle]
pub extern "C" fn rustc_jubjub_zero(buffer: *mut [c_uchar; LENGTH_JUBJUB_AFFINE_POINT]) {
    let g1 = AffinePoint::identity();
    write_jubjub_affine_point(buffer, g1)
}

#[no_mangle]
pub extern "C" fn rustc_jubjub_one(buffer: *mut [c_uchar; LENGTH_JUBJUB_AFFINE_POINT]) {
    // From lib.rs
    let gen = AffinePoint::from(
        AffinePoint::from_raw_unchecked(
            Fq::from_raw([
                0xe4b3d35df1a7adfe,
                0xcaf55d1b29bf81af,
                0x8b0f03ddd60a8187,
                0x62edcbb8bf3787c8,
            ]),
            Fq::from_raw([0xb, 0x0, 0x0, 0x0]),
        )
        .mul_by_cofactor(),
    );
    write_jubjub_affine_point(buffer, gen);
}

#[no_mangle]
pub extern "C" fn rustc_jubjub_is_zero(g1: *const [c_uchar; LENGTH_JUBJUB_AFFINE_POINT]) -> bool {
    let g1 = unsafe { &*g1 };
    // g1 and g2 are supposed to be on the curve
    let g1 = AffinePoint::from_bytes(*g1).unwrap();
    bool::from(g1 == AffinePoint::identity())
}

#[no_mangle]
pub extern "C" fn rustc_jubjub_add(
    buffer: *mut [c_uchar; LENGTH_JUBJUB_AFFINE_POINT],
    g1: *const [c_uchar; LENGTH_JUBJUB_AFFINE_POINT],
    g2: *const [c_uchar; LENGTH_JUBJUB_AFFINE_POINT],
) {
    let g1 = unsafe { &*g1 };
    let g2 = unsafe { &*g2 };
    // g1 and g2 are supposed to be on the curve
    let g1 = AffinePoint::from_bytes(*g1).unwrap();
    let g2 = AffinePoint::from_bytes(*g2).unwrap();

    let g1_ext = ExtendedPoint::from(g1);
    let g2_ext = ExtendedPoint::from(g2);
    let sum_ext = g1_ext.add(g2_ext);
    let sum_aff = AffinePoint::from(sum_ext);
    write_jubjub_affine_point(buffer, sum_aff)
}

#[no_mangle]
pub extern "C" fn rustc_jubjub_double(
    buffer: *mut [c_uchar; LENGTH_JUBJUB_AFFINE_POINT],
    g: *const [c_uchar; LENGTH_JUBJUB_AFFINE_POINT],
) {
    let g = unsafe { &*g };
    let g = AffinePoint::from_bytes(*g).unwrap();

    let g_ext = ExtendedPoint::from(g);
    let double_g_ext = g_ext.double();
    write_jubjub_affine_point(buffer, AffinePoint::from(double_g_ext))
}

#[no_mangle]
pub extern "C" fn rustc_jubjub_negate(
    buffer: *mut [c_uchar; LENGTH_JUBJUB_AFFINE_POINT],
    g: *const [c_uchar; LENGTH_JUBJUB_AFFINE_POINT],
) {
    let g = unsafe { &*g };
    let g = AffinePoint::from_bytes(*g).unwrap();
    write_jubjub_affine_point(buffer, -g)
}

#[no_mangle]
pub extern "C" fn rustc_jubjub_eq(
    g1: *const [c_uchar; LENGTH_JUBJUB_AFFINE_POINT],
    g2: *const [c_uchar; LENGTH_JUBJUB_AFFINE_POINT],
) -> bool {
    let g1 = unsafe { &*g1 };
    let g2 = unsafe { &*g2 };
    // g1 and g2 are supposed to be on the curve
    let g1 = AffinePoint::from_bytes(*g1).unwrap();
    let g2 = AffinePoint::from_bytes(*g2).unwrap();
    bool::from(g1 == g2)
}

#[no_mangle]
pub extern "C" fn rustc_jubjub_mul(
    buffer: *mut [c_uchar; LENGTH_JUBJUB_AFFINE_POINT],
    g: *const [c_uchar; LENGTH_JUBJUB_AFFINE_POINT],
    a: *const [c_uchar; LENGTH_JUBJUB_FR],
) {
    let g = unsafe { &*g };
    let a = unsafe { &*a };
    let g = AffinePoint::from_bytes(*g).unwrap();
    let a = Fr::from_bytes(&*a).unwrap();
    let mut res = ExtendedPoint::from(g);
    res *= &a;
    let res = AffinePoint::from(res);
    write_jubjub_affine_point(buffer, res);
}

#[no_mangle]
pub extern "C" fn rustc_jubjub_get_u_coordinate(
    buffer: *mut [c_uchar; LENGTH_JUBJUB_FQ],
    g: *const [c_uchar; LENGTH_JUBJUB_AFFINE_POINT],
) {
    let g = unsafe { &*g };
    let g = AffinePoint::from_bytes(*g).unwrap();
    let u = g.get_u();
    write_jubjub_coordinate(buffer, u)
}

#[no_mangle]
pub extern "C" fn rustc_jubjub_get_v_coordinate(
    buffer: *mut [c_uchar; LENGTH_JUBJUB_FQ],
    g: *const [c_uchar; LENGTH_JUBJUB_AFFINE_POINT],
) {
    let g = unsafe { &*g };
    let g = AffinePoint::from_bytes(*g).unwrap();
    let x = g.get_v();
    write_jubjub_coordinate(buffer, x)
}

#[no_mangle]
pub extern "C" fn rustc_jubjub_from_coordinates(
    buffer: *mut [c_uchar; LENGTH_JUBJUB_AFFINE_POINT],
    u: *const [c_uchar; LENGTH_JUBJUB_FQ],
    v: *const [c_uchar; LENGTH_JUBJUB_FQ],
) {
    let u = unsafe { &*u };
    let v = unsafe { &*v };
    let g =
        AffinePoint::from_raw_unchecked(Fq::from_bytes(&*u).unwrap(), Fq::from_bytes(&*v).unwrap());
    write_jubjub_affine_point(buffer, g)
}

#[no_mangle]
pub extern "C" fn rustc_jubjub_is_torsion_free(
    p: *const [c_uchar; LENGTH_JUBJUB_AFFINE_POINT],
) -> bool {
    let p = unsafe { &*p };
    let p = AffinePoint::from_bytes(*p).unwrap();
    bool::from(p.is_torsion_free())
}

#[no_mangle]
pub extern "C" fn rustc_jubjub_multiply_by_cofactor(
    buffer: *mut [c_uchar; LENGTH_JUBJUB_AFFINE_POINT],
    g: *const [c_uchar; LENGTH_JUBJUB_AFFINE_POINT],
) {
    let g = unsafe { &*g };
    let g = AffinePoint::from_bytes(*g).unwrap();
    let g_ext = g.mul_by_cofactor();
    write_jubjub_affine_point(buffer, AffinePoint::from(g_ext))
}
