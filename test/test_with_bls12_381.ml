module Groth16_BLS12_381 = Groth16.Generic.MakeGroth16 (struct
  module G1 = Bls12_381.G1.Uncompressed
  module G2 = Bls12_381.G2.Uncompressed
  module GT = Bls12_381.Fq12
  include Bls12_381.Pairing
end)
